package com.loadtest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.example.client.service.CalciClient;
import com.example.common.Results;
import com.example.common.Values;
import com.loadtest.response.Response;
import com.loadtest.response.Status;

public class TestManager {
  private static final Logger logger = LoggerFactory.getLogger(TestManager.class);

  public Response SequentialTesting(Values value) throws IOException {
    List<Status> datalist = new ArrayList<Status>();
    int failure = 0, totalcalls;
    for (totalcalls = 0; totalcalls < 20; totalcalls++) {
      Status status = new Status();
      Results result = new Results();
      status.setValue(value);
      CalciClient calciclient = new CalciClient();
      try {
        logger.info("connecting to client");
        result = calciclient.Connect_Server(value);
        status.setResponse("" + result.getResult());
        status.setStatus("Success");
        logger.info("Success");
        datalist.add(status);
      } catch (org.apache.http.client.ClientProtocolException exception) {
        failure++;
        logger.warn("Failure");
        status.setResponse(exception.toString());
        status.setStatus("failure");
        datalist.add(status);
      }
    }
    Response response = new Response((totalcalls - failure), failure,
        " Total of " + (totalcalls - failure) + " calls executed out of " + totalcalls + " calls",
        datalist);
    return response;
  }

}
