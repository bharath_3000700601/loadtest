package com.loadtest;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.common.Values;
import com.loadtest.response.Response;


@RestController
public class LoadTestApplication {

  private static final Logger logger = LoggerFactory.getLogger(LoadTestApplication.class);


  @RequestMapping(method = RequestMethod.POST, value = "/calci")
  public Response SequentialTesting(@RequestBody Values value) throws IOException {

    TestManager testmanager = new TestManager();
    Response response = testmanager.SequentialTesting(value);
    return response;
  }



}
