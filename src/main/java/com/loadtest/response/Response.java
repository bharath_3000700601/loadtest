package com.loadtest.response;

import java.util.List;

public class Response {
  int success;
  int fail;
  String Message;
  List<Status> data;
  
  public Response() {
    
  }
  public Response(int success, int fail, String message, List<Status> data) {
    super();
    this.success = success;
    this.fail = fail;
    Message = message;
    this.data = data;
  }
  public int getSuccess() {
    return success;
  }
  public void setSuccess(int success) {
    this.success = success;
  }
  public int getFail() {
    return fail;
  }
  public void setFail(int fail) {
    this.fail = fail;
  }
  public String getMessage() {
    return Message;
  }
  public void setMessage(String message) {
    Message = message;
  }
  public List<Status> getData() {
    return data;
  }
  public void setData(List<Status> data) {
    this.data = data;
  }
 
  

}
