package com.loadtest.response;

import com.example.common.Values;

public class Status {
      Values value;
      String response;
      String status;
      public Status() {
      
      }
      
      public Status(Values value, String response, String status) {
        super();
        this.value = value;
        this.response = response;
        this.status = status;
      }

      public Values getValue() {
        return value;
      }
      public void setValue(Values value) {
        this.value = value;
      }
      public String getResponse() {
        return response;
      }
      public void setResponse(String response) {
        this.response = response;
      }
      public String getStatus() {
        return status;
      }
      public void setStatus(String status) {
        this.status = status;
      }
      
      

      
      
}
